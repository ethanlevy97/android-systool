# Main makefile for toolkit
# Copyright (c) 2015 Ethan Levy <eitanlevy97@yandex.com>

.PHONY: all
all:
	@-mkdir build
	@$(MAKE) -C ext4_utils
	@cp -f ext4_utils/{ext2simg,make_ext4fs,ext4fixup,mkuserimg.sh} build
	@mv build/mkuserimg.sh build/mkuserimg
	@$(MAKE) -C libsparse
	@cp -f libsparse/{img2simg,simg2img,simg2simg,append2simg} build
	@$(MAKE) -C mkbootimg
	@cp -f mkbootimg/{mkbootimg,unpackbootimg} build

.PHONY: install
install:
	@install -m 0755 -s build/{ext2simg,make_ext4fs,ext4fixup,img2simg,simg2img,simg2simg,append2simg,mkbootimg,unpackbootimg} $(PREFIX)/bin
	@install -m 0755 build/mkuserimg $(PREFIX)/bin

.PHONY: uninstall
	@rm -f $(PREFIX)/bin/{ext2simg,make_ext4fs,ext4fixup,mkuserimg,img2simg,simg2img,simg2simg,append2simg,mkbootimg,unpackbootimg}

.PHONY: clean
clean:
	@-rm -rf build
	@-$(MAKE) -C ext4_utils clean
	@-$(MAKE) -C libcutils clean
	@-$(MAKE) -C libmincrypt clean
	@-$(MAKE) -C libselinux clean
	@-$(MAKE) -C libsparse clean
	@-$(MAKE) -C log clean
	@-$(MAKE) -C mkbootimg clean